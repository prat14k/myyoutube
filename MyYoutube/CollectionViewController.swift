//
//  CollectionViewController.swift
//  MyYoutube
//
//  Created by Prateek on 10/11/17.
//  Copyright © 2017 14K. All rights reserved.
//

import UIKit

private let reuseIdentifier = "videoCell"

class CollectionViewController: UIViewController, UICollectionViewDelegate , UICollectionViewDataSource,  UICollectionViewDelegateFlowLayout {

    @IBOutlet var collectionView : UICollectionView!
    @IBOutlet var slidingTabSelectionView : UIView! {
        didSet {
            slidingTabSelectionView.tag = 1
            slidingTabSelectionView.transform = CGAffineTransform.identity
        }
    }
    
    let tabNames = ["Home", "Trending", "Subscriptions", "Account"]
    lazy var navBarTitle : UILabel! = {
        let lbl = UILabel()
        lbl.text = self.tabNames[0];
        lbl.textColor = UIColor.white
        lbl.font = UIFont.systemFont(ofSize: 17)
        lbl.sizeToFit()
        lbl.frame = CGRect(x: 0, y: 0, width: self.view.frame.width-30, height: lbl.frame.height)
        
        return lbl
    }()
    
    
    @IBOutlet weak var tab1HomeBtn : UIButton! {
        didSet {
            tab1HomeBtn.setTitle(kicon_home, for: .normal)
        }
    }
    @IBOutlet weak var tab2TrendingBtn : UIButton! {
        didSet {
            tab2TrendingBtn.setTitle(kicon_trending, for: .normal)
        }
    }
    @IBOutlet weak var tab3SubscriptionBtn : UIButton! {
        didSet {
            tab3SubscriptionBtn.setTitle(kicon_subscription, for: .normal)
        }
    }
    @IBOutlet weak var tab4AccountBtn : UIButton! {
        didSet {
            tab4AccountBtn.setTitle(kicon_account, for: .normal)
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

//
//        self.collectionView!.register(UICollectionViewCell.self, forCellWithReuseIdentifier: reuseIdentifier)
        
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.hidesBarsOnSwipe = true
        
        tab1HomeBtn.setTitleColor(UIColor.white, for: .normal)
    
        self.navigationItem.titleView = navBarTitle
        
       // setupMenubar()
    }

    let menuBar : MenuBar = {
       
        let mb = MenuBar()
        return mb
        
    }()
    
    func setupMenubar(){
        
        view.addSubview(menuBar)
        view.addConstraint(NSLayoutConstraint(item: menuBar, attribute: .top, relatedBy: .equal, toItem: view, attribute: .top, multiplier: 1.0, constant: 0))
        view.addConstraint(NSLayoutConstraint(item: menuBar, attribute: .centerX, relatedBy: .equal, toItem: view, attribute: .centerX, multiplier: 1.0, constant: 0))
        view.addConstraint(NSLayoutConstraint(item: menuBar, attribute: .width, relatedBy: .equal, toItem: view, attribute: .width, multiplier: 1.0, constant: 0))
        view.addConstraint(NSLayoutConstraint(item: menuBar, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 50))
        
    }
    
    // MARK: UICollectionViewDataSource

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return 1
    }


    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return 6
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath)
    
        
    
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let hght = (self.view.frame.width - 30) * (9 / 16)
        
        return CGSize(width: self.view.frame.width, height: hght + 10 + 45 + 16 + 12)
        
    }
    
    @IBAction func tabChange(_ sender: UIButton){
        
        var transform : CGAffineTransform = CGAffineTransform.identity
        
        tab1HomeBtn.setTitleColor(tabUnselectedColor, for: .normal)
        tab2TrendingBtn.setTitleColor(tabUnselectedColor, for: .normal)
        tab3SubscriptionBtn.setTitleColor(tabUnselectedColor, for: .normal)
        tab4AccountBtn.setTitleColor(tabUnselectedColor, for: .normal)
        
        navBarTitle.text = tabNames[sender.tag - 1]
        switch sender.tag {
        case 1:
            tab1HomeBtn.setTitleColor(UIColor.white, for: .normal)
            transform = CGAffineTransform.identity
            break
        case 2:
            tab2TrendingBtn.setTitleColor(UIColor.white, for: .normal)
            transform = CGAffineTransform(translationX: tab2TrendingBtn.frame.origin.x, y: 0)
            break
        case 3:
            tab3SubscriptionBtn.setTitleColor(UIColor.white, for: .normal)
            transform = CGAffineTransform(translationX: tab3SubscriptionBtn.frame.origin.x, y: 0)
            break
        case 4:
            tab4AccountBtn.setTitleColor(UIColor.white, for: .normal)
            transform = CGAffineTransform(translationX: tab4AccountBtn.frame.origin.x, y: 0)
            break
        default:
            break
        }
        
        if sender.tag != slidingTabSelectionView.tag {
            slidingTabSelectionView.tag = sender.tag
            UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseOut, animations: {
                self.slidingTabSelectionView.transform = transform
            }, completion: nil)
        }
        
    }
    
}
