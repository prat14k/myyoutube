//
//  MenuBar.swift
//  MyYoutube
//
//  Created by Prateek on 16/11/17.
//  Copyright © 2017 14K. All rights reserved.
//

import UIKit

class MenuBar: UIView {

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        translatesAutoresizingMaskIntoConstraints = false
        backgroundColor = UIColor(red: 200/255, green: 32/255, blue: 31/255, alpha: 1.0)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
