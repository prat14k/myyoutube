//
//  Constants.swift
//  MyYoutube
//
//  Created by Prateek Sharma on 16/12/17.
//  Copyright © 2017 14K. All rights reserved.
//

import UIKit

let kicon_trending = "\u{e800}"
let kicon_home = "\u{e801}"
let kicon_account = "\u{e802}"
let kicon_subscription = "\u{e803}"

let tabUnselectedColor = UIColor(red: 99.0/255.0, green: 6.0/255.0, blue: 11.0/255.0, alpha: 1.0)
let navBarColor = UIColor(red: 255/255, green: 41/255, blue: 10/255, alpha: 1.0)

class Constants: NSObject {

}
